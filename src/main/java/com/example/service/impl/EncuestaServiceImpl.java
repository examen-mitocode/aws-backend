package com.example.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.EncuestaDTO;
import com.example.entities.EncuestaEntity;
import com.example.repository.EncuestaRepository;
import com.example.service.IEncuestaService;

@Service
public class EncuestaServiceImpl implements IEncuestaService {

	@Autowired
	private EncuestaRepository repo;

	@Override
	@Transactional
	public EncuestaDTO save(EncuestaDTO dto) {
		EncuestaEntity entity = repo.save(EncuestaEntity.buildEntity(dto));
		if (entity != null)
			return entity.toDTO();
		else
			return null;
	}

	@Override
	public List<EncuestaDTO> findAll() {
		List<EncuestaEntity> listado = repo.findAll();
		List<EncuestaDTO> dtos = new ArrayList<>();
		if (listado != null && listado.size() > 0)
			listado.stream().forEach((item) -> {
				dtos.add(item.toDTO());
			});
		return dtos;
	}

}
