package com.example.service;

import java.util.List;

import com.example.dto.EncuestaDTO;

public interface IEncuestaService {

	EncuestaDTO save(EncuestaDTO dto);

	List<EncuestaDTO> findAll();
}
