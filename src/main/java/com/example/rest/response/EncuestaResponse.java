package com.example.rest.response;

import java.util.List;

import com.example.dto.BaseDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EncuestaResponse <T>{

	private boolean success;
	private BaseDTO obj;
	private List<T> lObj;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public BaseDTO getObj() {
		return obj;
	}

	public void setObj(BaseDTO obj) {
		this.obj = obj;
	}

	public List<T> getlObj() {
		return lObj;
	}

	public void setlObj(List<T> lObj) {
		this.lObj = lObj;
	}

}
