package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.BaseDTO;
import com.example.dto.EncuestaDTO;
import com.example.rest.response.EncuestaResponse;
import com.example.service.IEncuestaService;

@RestController
@RequestMapping("api")
public class EncuestaController {

	@Autowired
	private IEncuestaService encuestaService;

	@PostMapping(value = "/encuesta")
	public ResponseEntity<EncuestaResponse> save(@RequestBody EncuestaDTO dto) {
		EncuestaResponse restResponse = new EncuestaResponse();
		EncuestaDTO dtoResponse = encuestaService.save(dto);
		if (dtoResponse != null) {
			restResponse.setSuccess(true);
			restResponse.setObj(dtoResponse);
			return new ResponseEntity<EncuestaResponse>(restResponse, HttpStatus.OK);
		} else {
			restResponse.setSuccess(false);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/encuesta")
	public ResponseEntity<EncuestaResponse> findAll() {
		EncuestaResponse<EncuestaDTO> restResponse = new EncuestaResponse();
		List<EncuestaDTO> dtoResponse = encuestaService.findAll();
		if (dtoResponse != null) {
			restResponse.setSuccess(true);
			restResponse.setlObj(dtoResponse);
			return new ResponseEntity<EncuestaResponse>(restResponse, HttpStatus.OK);
		} else {
			restResponse.setSuccess(false);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
