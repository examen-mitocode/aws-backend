package com.example;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories
@EnableWebMvc 
public class ExamenApplication extends WebMvcConfigurerAdapter{
	
	@Value("${aws_region_cognito}")
	private String awsCognitoRegion;

	@Value("${AWS_ACCESS_KEY_ID}")
	private String accessKey;

	@Value("${AWS_SECRET_ACCESS_KEY}")
	private String secretKey;

	public static void main(String[] args) {
		SpringApplication.run(ExamenApplication.class, args);
	}



	@Bean
	public AWSCognitoIdentityProviderClient CognitoClient() {        
		BasicAWSCredentials basicCredentials = new BasicAWSCredentials(accessKey, secretKey);
        AWSCognitoIdentityProviderClient cognitoClient = new AWSCognitoIdentityProviderClient(basicCredentials);
        cognitoClient.setRegion(Region.getRegion(Regions.fromName(awsCognitoRegion)));
        return cognitoClient;
	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
	 	registry.addMapping("/**").allowedOrigins("*");
	}
}
