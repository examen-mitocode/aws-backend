package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.example.dto.EncuestaDTO;

@Entity
@Table(name = "encuesta")
public class EncuestaEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nombres;
	private String apellidos;
	private Integer edad;
	private String profesion;
	@Column(name = "lugar_de_trabajo")
	private String lugarDeTrabajo;
	@Column(name = "resultado_encuesta")
	private String resultadoEncuesta;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getLugarDeTrabajo() {
		return lugarDeTrabajo;
	}

	public void setLugarDeTrabajo(String lugarDeTrabajo) {
		this.lugarDeTrabajo = lugarDeTrabajo;
	}

	public String getResultadoEncuesta() {
		return resultadoEncuesta;
	}

	public void setResultadoEncuesta(String resultadoEncuesta) {
		this.resultadoEncuesta = resultadoEncuesta;
	}

	public static EncuestaEntity buildEntity(EncuestaDTO dto) {
		EncuestaEntity entity = new EncuestaEntity();
		BeanUtils.copyProperties(dto, entity);
		return entity;
	}

	public EncuestaDTO toDTO() {
		EncuestaDTO dto = new EncuestaDTO();
		BeanUtils.copyProperties(this, dto);
		return dto;
	}

}
