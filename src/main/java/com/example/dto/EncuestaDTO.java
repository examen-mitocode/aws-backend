package com.example.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EncuestaDTO implements BaseDTO {

	private Long id;
	@JsonProperty("nombres")
	private String nombres;

	@JsonProperty("apellidos")
	private String apellidos;
	private Integer edad;
	private String profesion;
	private String lugarDeTrabajo;
	private String resultadoEncuesta;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getLugarDeTrabajo() {
		return lugarDeTrabajo;
	}

	public void setLugarDeTrabajo(String lugarDeTrabajo) {
		this.lugarDeTrabajo = lugarDeTrabajo;
	}

	public String getResultadoEncuesta() {
		return resultadoEncuesta;
	}

	public void setResultadoEncuesta(String resultadoEncuesta) {
		this.resultadoEncuesta = resultadoEncuesta;
	}

}
