package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entities.EncuestaEntity;

public interface EncuestaRepository extends JpaRepository<EncuestaEntity, Long> {

}
